#ifndef BREADTHFIRSTSEARCH_H
#define BREADTHFIRSTSEARCH_H

#include "graph.h"

class Breadthfirstsearch
{
public:
    Breadthfirstsearch();
    void search(Graph *graph);
};

#endif // BREADTHFIRSTSEARCH_H
