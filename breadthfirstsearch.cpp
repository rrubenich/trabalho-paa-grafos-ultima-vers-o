#include "breadthfirstsearch.h"

#include "lista.h"

void Breadthfirstsearch::search(Graph *graph){

    Lista<Vertex> fifo;

    Vertex **v = graph->getVertex();
    Vertex *vertexProc, *vertexAux;
    Edge *edge;
    int i;

    for (i = 0; i < graph->getVertexCount(); i++) {
        v[i]->setFather(NULL);
        v[i]->setColor(Qt::white);
        v[i]->setD(-1);
    }

    v[0]->setColor(Qt::gray);
    v[0]->setD(0);

    fifo.insert(*v[0]);

    while(!fifo.vazia()) {
        vertexProc = fifo.removeFirst();

        for(edge = vertexProc->getEdges();edge != NULL; edge = edge->getNext()) {

            vertexAux = v[edge->getIdAdj()];

            if(vertexAux->getColor() == Qt::white) {

                vertexAux->setColor(Qt::gray);
                vertexAux->setFather(vertexProc);
                vertexAux->setD(vertexProc->getD()+1);
                fifo.insert(*vertexAux);
            }
        }

        vertexProc->setColor(Qt::black);
    }
}
