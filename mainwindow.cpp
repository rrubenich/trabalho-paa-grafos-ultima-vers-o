#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFile>
#include <QFileDialog>
#include <QMessageBox>

#include "breadthfirstsearch.h"

#define KRUSKAL 5

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {

    ui->setupUi(this);

    this->ui->toolBar->addWidget( this->ui->labelInicial );
    this->ui->toolBar->addWidget( this->ui->cbOrigem );
    this->ui->statusBar->addWidget( this->ui->labelFinal  );
    this->ui->statusBar->addWidget( this->ui->cbFinal );
    this->ui->statusBar->addWidget( this->ui->textEdit );
    this->ui->statusBar->addWidget( this->ui->pushButton );

    QMainWindow::paintEvent(new QPaintEvent(this->geometry()));

    this->graph=new Graph(0, this);
    this->tmp=NULL;

    connect( this, SIGNAL (mostrar( Graph* )), this, SLOT ( mostrarGrafo(Graph* ) ) );


    this->alg = NULL;

    idmostrar=0;
}

void MainWindow::createAlgoritm ( int i ) {
    if (alg) delete alg;
    switch (i) {
    case KRUSKAL : this->alg = new kruskal ( graph, this, this ); break;
    }
    connect( alg,  SIGNAL ( update(Graph*)), this, SLOT ( mostrarGrafo(Graph* ) ) );
    alg->start();
}

void MainWindow::paintEvent(QPaintEvent *) {
    graph->paint();
    if (!this->tmp) return;
    tmp->paint();
}

void MainWindow::mostrarGrafo ( Graph *g ) {
    this->tmp = g;
    update ();
}

void MainWindow::on_actionLoad_triggered() {
    QDir::setCurrent("../files");
    qDebug() << QDir::currentPath();
    QString filename =  QFileDialog::getOpenFileName( this, tr("Open Document"),
                                                      QDir::currentPath(),
                                                      tr("Document files (*.txt);All files (*.*)"), 0,
                                                      QFileDialog::DontUseNativeDialog );
    if( !filename.isNull() ) {
        //Commented because it not work
        //qDebug() << filename.toAscii();
        QString s = graph->loadFromFile(filename);
        ui->cbOrigem->addItems( s.split(";"));
        ui->cbFinal->addItems( s.split( ";") );
        emit mostrar ( graph );
    }
}

void MainWindow::on_actionBusca_em_profundidade_triggered() {
    //if (this->graph!=NULL) graph->dfs();
}

void MainWindow::on_pushButton_clicked() {
    //if (this->graph!=NULL)
//        this->ui->textEdit->setText( this->graph->getCaminho( this->ui->cbFinal->currentText() ));
}

void MainWindow::on_actionBusca_em_largura_triggered() {
    if (this->graph!=NULL){
        Breadthfirstsearch bfs();
        //bfs.search(this->graph);
    }
}

void MainWindow::on_actionDijkstra_triggered() {
  //  if (this->graph!=NULL) graph->dijkstra(
    //            graph->getVerticeIndex(this->ui->cbOrigem->currentText()));
}

void MainWindow::on_actionKRUSKAL_triggered() {
     if (this->graph!=NULL) {
         createAlgoritm( KRUSKAL );
        }
}

void MainWindow::on_actionOrdena_o_Topol_gica_triggered() {
    /*Lista *l;
    if (this->graph!=NULL) {
        graph->dfs(&l);
        ui->textEdit->setText( l->getLista () );
    }*/

}

MainWindow::~MainWindow() {
    if (graph) delete graph;
    if (alg) delete alg;
    delete ui;
}
