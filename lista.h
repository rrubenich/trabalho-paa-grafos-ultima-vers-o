#include <iostream>
#include <vector>

using namespace std;

template <class T>

class Lista {
private:

    typedef struct celula{
        T item;
        celula* next;
    }* tipoCelula;

    tipoCelula head; //Cabeça da lista
    tipoCelula aux;  //Auxiliar

public:
    Lista(){
        head = NULL;
        aux = NULL;
    }

    void insert(T novo){
        //Cria o objeto de nó
        tipoCelula n = new celula;
        n->next = NULL;
        n->item = novo;

        //Roda a lista para adiconar no ultimo
        if(head != NULL){
            aux = head;
            while(aux->next != NULL){
                aux = aux->next;
            }

            aux->next = n;
        }
        //Se não, adiciona na cabeça
        else{
            head = n;
        }
    }

    bool vazia(){
        if(head == NULL) return true;
        else return false;
    }

    T *removeFirst(){
        aux = head;
        head = head->next;
        return aux->item;
    }

};
