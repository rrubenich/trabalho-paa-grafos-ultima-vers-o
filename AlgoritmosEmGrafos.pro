#-------------------------------------------------
#
# Project created by QtCreator 2014-06-26T11:25:21
#
#-------------------------------------------------

QT       += gui declarative

TARGET = AlgoritmosEmGrafos
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \    
    vertex.cpp \
    graph.cpp \
    edge.cpp \
    kruskal.cpp \
    depthfirstsearch.cpp \
    breadthfirstsearch.cpp \
    dijkstra.cpp \
    prim.cpp \
    fordfulkerson.cpp \
    topologicalsorting.cpp

HEADERS  += mainwindow.h \
    vertex.h \
    list.h \
    graph.h \
    edge.h \
    kruskal.h \
    depthfirstsearch.h \
    breadthfirstsearch.h \
    dijkstra.h \
    prim.h \
    fordfulkerson.h \
    topologicalsorting.h \
    lista.h

FORMS    += mainwindow.ui
